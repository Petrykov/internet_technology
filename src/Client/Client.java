package Client;

import Client.MessageTypes.*;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Scanner;

public class Client {

    public final static int SOCKET_PORT = 1339;
    public final static String SERVER = "127.0.0.1";

    private static Scanner scanner = new Scanner(System.in);
    private static PrintWriter writer;
    private Socket socket;
    private Receive_MSG receiveMessages;
    private static boolean validCredentials = false;
    private String input;
    private boolean is_empty = true;
    private OutputStream outputStream;
    private static PrivateKey privateKey;
    private String encoded;
    private Base64.Encoder encoder = Base64.getEncoder();
    private static Base64.Decoder decoder = Base64.getDecoder();

    private static Cipher cipher;
    boolean send_key = false;

    public Client() throws NoSuchAlgorithmException, NoSuchPaddingException {

        try {
            socket = new Socket(SERVER, SOCKET_PORT);
            outputStream = socket.getOutputStream();
            writer = new PrintWriter(outputStream);
            receiveMessages = new Receive_MSG(socket.getInputStream(), writer);
            receiveMessages.showServerMessages(true);
            new Thread(receiveMessages).start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        cipher = Cipher.getInstance("RSA");
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(2048);
        KeyPair pair = keyGen.generateKeyPair();
        privateKey = pair.getPrivate();

        PublicKey publicKey = pair.getPublic();
        encoded = Base64.getEncoder().encodeToString(publicKey.getEncoded());

    }


    public String encryptText(String msg, PublicKey key)
            throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return encoder.encodeToString(cipher.doFinal(msg.getBytes(StandardCharsets.UTF_8)));
    }

    public static String decryptText(String msg, PrivateKey key)
            throws InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException {
        cipher.init(Cipher.DECRYPT_MODE, key);
        return new String(cipher.doFinal(decoder.decode(msg)), StandardCharsets.UTF_8);
    }

    public static void main(String[] args) {
        try {
            new Client().run();
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }

    public void run() throws BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, UnsupportedEncodingException, InvalidKeyException, InvalidKeySpecException {

        while (socket.isConnected()) {

            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Message.messageType messageType = receiveMessages.getType();

            switch (messageType) {

                case HELO:
                    caseHello();
                    break;

                case MENU:
                    if (socket.getPort() != 1337 && !send_key) {
                        MSG_PUBLIC_KEY messageKeys = new MSG_PUBLIC_KEY(encoded);
                        msgToServer(messageKeys);
                    }
                    send_key = true;
                    Menu menu = new Menu(this);
                    menu.run();
                    break;

                case ALREADY_EXISTS:
                    caseAlreadyExists();
                    break;

                case INVALID_FORMAT:
                    caseInvalidFormat();
                    break;

                case BROADCAST:
                    caseBroadcast();
                    break;

                case KICKED:
                    caseKicked();
                    break;

                case PUBLIC_KEY:
                    casePublicKeyReceive();
                    break;

            }
        }
        System.out.println("server is gone");
    }

    public static PrivateKey getPrivateKey() {
        return privateKey;
    }

    private boolean leaveGroupCheck(String msg) {
        return msg.equals("QUIT");
    }

    private void quitServerCheck(String msg) {
        if (msg.equals("QUIT")) {
            writer.println("QUIT");
            writer.flush();
            try {
                System.out.println("Bye Bye!");
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.exit(-1);
        }
    }

    public boolean isEmpty(String text) {
        boolean to_return;
        if (text.isEmpty()) {
            System.out.print("Input can't be empty: ");
            to_return = true;
        } else {
            to_return = false;
        }
        return to_return;
    }

    public String readStringInput() {
        return scanner.nextLine();
    }

    public void msgToServer(Server_MSG msg) {
        writer.println(msg.toString());
        writer.flush();
    }

    public String validateInput(String input) {
        validCredentials = validInputFormat(input);

        while (!validCredentials) {
            System.out.print("Please enter the right credentials: ");
            input = readStringInput();
            validCredentials = validInputFormat(input);
        }
        return input;
    }

    public boolean validInputFormat(String input) {
        boolean toReturn;

        if (input.length() > 3 && input.matches("[a-zA-Z0-9_]{3,14}")) {
            toReturn = true;
        } else {
            toReturn = false;
        }
        return toReturn;
    }

    private void caseHello() {
        System.out.print("Enter your name: ");

        input = readStringInput();
        if (!validCredentials) {
            input = validateInput(input);
        }
        MSG_HELO msg_helo = new MSG_HELO(input);
        msgToServer(msg_helo);

        validCredentials = false;
    }

    private void caseAlreadyExists() {
        System.out.print("User already exists, try another one: ");

        while (is_empty) {
            input = readStringInput();
            is_empty = isEmpty(input);
        }

        if (!validCredentials) {
            input = validateInput(input);
        }
        MSG_HELO msg = new MSG_HELO(input);
        msgToServer(msg);

        is_empty = true;
        validCredentials = false;
    }

    private void caseInvalidFormat() {
        System.out.println("Server side error");

        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.exit(-1);
    }

    private void caseBroadcast() {
        System.out.println("Broadcast your message: ");

        String group_message = "";
        group_message += readStringInput();

        if (socket.getPort() == 1337) {

            quitServerCheck(group_message);
            MSG_BCST msg_bcst = new MSG_BCST(group_message);
            msgToServer(msg_bcst);

        } else {
            boolean wanna_leave = leaveGroupCheck(group_message);

            if (wanna_leave) {
                MSG_LEAVE msg_leave = new MSG_LEAVE(group_message);
                msgToServer(msg_leave);

            } else if (group_message.startsWith("KICK")) {
                MSG_KICK msg_kick = new MSG_KICK(group_message);
                msgToServer(msg_kick);

            } else {
                MSG_BCST msg_leave = new MSG_BCST(group_message);
                msgToServer(msg_leave);
            }

        }
    }

    private void caseKicked() {
        System.out.println("You was kicked from the group by the admin");
        MSG_LEAVE msg_leave = new MSG_LEAVE("");
        msgToServer(msg_leave);
    }

    private void casePublicKeyReceive() throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

        String public_key = receiveMessages.getServerMessage(1);

        byte[] publicBytes = decoder.decode(public_key);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey pubKey = keyFactory.generatePublic(keySpec);

        System.out.print("Write a message to send: ");
        String message = readStringInput();
        String encrypted = encryptText(message, pubKey);

        MSG_DIRECT_MESSAGE direct_msg = new MSG_DIRECT_MESSAGE(encrypted);
        msgToServer(direct_msg);
    }

    public boolean isValidCredentials() {
        return validCredentials;
    }

    public void setValidCredentials(boolean status) {
        validCredentials = status;
    }

    public OutputStream getOutputStream() {
        return outputStream;
    }
}