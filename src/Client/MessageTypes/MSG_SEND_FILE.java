package Client.MessageTypes;

public class MSG_SEND_FILE extends Server_MSG {

    private String receiver;

    public MSG_SEND_FILE(String receiver){
        this.receiver = receiver;
    }

    @Override
    public String toString() {
        return "FILE_TRANSFER " + receiver;
    }
}
