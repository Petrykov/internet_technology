package Client.MessageTypes;

public class MSG_KICK extends Server_MSG {

    private String user_to_kick;

    public MSG_KICK(String user_to_kick) {
        this.user_to_kick = user_to_kick;
    }

    @Override
    public String toString() {
        return user_to_kick;
    }
}
