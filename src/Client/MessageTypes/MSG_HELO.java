package Client.MessageTypes;

public class MSG_HELO extends Server_MSG {

    private String username;

    public MSG_HELO(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "HELO " + username;
    }

}
