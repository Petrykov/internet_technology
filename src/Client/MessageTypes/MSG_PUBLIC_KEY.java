package Client.MessageTypes;

public class MSG_PUBLIC_KEY extends Server_MSG {

    private String key;

    public MSG_PUBLIC_KEY(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "KEY " + key;
    }

}
