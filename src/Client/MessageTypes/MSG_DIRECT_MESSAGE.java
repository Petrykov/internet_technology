package Client.MessageTypes;

public class MSG_DIRECT_MESSAGE extends Server_MSG {

    private String receiver;

    public MSG_DIRECT_MESSAGE(String receiver) {
        this.receiver = receiver;
    }

    @Override
    public String toString() {
        return "DIRECT_MESSAGE " + receiver;
    }
}
