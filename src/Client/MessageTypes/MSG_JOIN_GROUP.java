package Client.MessageTypes;

public class MSG_JOIN_GROUP extends Server_MSG {

    private String group_name;

    public MSG_JOIN_GROUP(String group_name){
        this.group_name = group_name;
    }

    @Override
    public String toString() {
        return "JOIN_GROUP " + group_name;
    }
}
