package Client.MessageTypes;

public class MSG_QUIT extends Server_MSG {

    String message = "QUIT";

    @Override
    public String toString() {
        return message;
    }
}
