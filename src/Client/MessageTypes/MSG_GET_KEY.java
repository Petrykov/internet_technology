package Client.MessageTypes;

public class MSG_GET_KEY extends Server_MSG {

    private String receiver;

    public MSG_GET_KEY(String receiver) {
        this.receiver = receiver;
    }

    @Override
    public String toString() {
        return "GET_KEY " + receiver;
    }
}
