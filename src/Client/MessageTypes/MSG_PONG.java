package Client.MessageTypes;

public class MSG_PONG extends Server_MSG {

    public String name(String message){
        String[] name = message.split(" ");
        return name[1];
    }

    @Override
    public String toString() {
        return "PONG";
    }
}
