package Client.MessageTypes;

public class MSG_BCST extends Server_MSG {

    private String message;

    public MSG_BCST(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "BCST " + message;
    }
}
