package Client.MessageTypes;

public class MSG_CREATE_GROUP extends Server_MSG {

    private String group_name;

    public MSG_CREATE_GROUP(String group_name) {
        this.group_name = group_name;
    }

    @Override
    public String toString() {
        return "CREATE_GROUP " + group_name;
    }
}
