package Client.MessageTypes;

public class MSG_LEAVE extends Server_MSG {

    String message;


    public MSG_LEAVE(String message){
        this.message = message;
    }

    @Override
    public String toString() {
        return "QUIT " + message;
    }
}
