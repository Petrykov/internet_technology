package Client;

import Client.MessageTypes.*;

import java.io.*;

public class Menu {

    private Client client;
    private boolean empty_input = true;
    private String userAnswer;

    public Menu(Client client) {
        this.client = client;
    }


    public void run() {

        printMenu();

        userAnswer = client.readStringInput();

        switch (userAnswer) {

            case "1":

                case1();
                break;

            case "2":

                case2();
                break;


            case "3":

                case3();
                break;


            case "4":

                case4();
                break;


            case "5":

                case5();
                break;

            case "6": {
                case6();
                break;
            }

            default:

                System.out.println("SELECT FROM MENU (1-5)");

                break;
        }
    }

    private void case1() {
        MSG_GET_USERS msg_users = new MSG_GET_USERS();
        client.msgToServer(msg_users);
    }

    private void case2() {
        System.out.print("To which person you want to send a message?: ");

        while (empty_input) {
            userAnswer = client.readStringInput();
            empty_input = client.isEmpty(userAnswer);
        }

        MSG_GET_KEY get_key = new MSG_GET_KEY(userAnswer);
        client.msgToServer(get_key);

        this.empty_input = true;
    }

    private void case3() {
        System.out.print("Type the name of your group: ");

        while (empty_input) {
            userAnswer = client.readStringInput();
            empty_input = client.isEmpty(userAnswer);
        }

        if (!client.isValidCredentials()) {
            userAnswer = client.validateInput(userAnswer);
        }

        MSG_CREATE_GROUP new_group_msg = new MSG_CREATE_GROUP(userAnswer);
        client.msgToServer(new_group_msg);

        empty_input = true;
        client.setValidCredentials(false);
    }

    private void case4() {
        MSG_GET_GROUPS get_groups_msg = new MSG_GET_GROUPS();
        client.msgToServer(get_groups_msg);
    }

    private void case5() {
        System.out.print("Type the name of group you want to join: ");
        userAnswer = client.readStringInput();
        MSG_JOIN_GROUP join = new MSG_JOIN_GROUP(userAnswer);
        client.msgToServer(join);
    }

    private void case6() {
        sendFile();
    }

    private void sendFile() {
        System.out.print("To which person you want to send a file?: ");

        while (empty_input) {
            userAnswer = client.readStringInput();
            empty_input = client.isEmpty(userAnswer);
        }

        empty_input = true;
        MSG_SEND_FILE msg_send_file = new MSG_SEND_FILE(userAnswer);

        System.out.print("Enter the path of the file: ");
        String file_path;
        file_path = client.readStringInput();

        do {
            try {
                File file = new File(file_path);

                FileInputStream fis = new FileInputStream(file);
                BufferedInputStream bis = new BufferedInputStream(fis);
                byte[] message = new byte[(int) file.length()];
                bis.read(message, 0, message.length);

                PrintWriter writer = new PrintWriter(client.getOutputStream());
                writer.println(msg_send_file + " " + message.length);
                writer.flush();

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                client.getOutputStream().write(message, 0, message.length);
                client.getOutputStream().flush();

                bis.close();
                fis.close();

                break;
            } catch (FileNotFoundException e) {
                System.out.print("Enter correct filepath: ");
                file_path = client.readStringInput();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (true);
    }

    public void printMenu() {
        System.out.println("\nChoose from menu : " +
                "\n1.Show users " +
                "\n2.Send direct message" +
                "\n3.Add group" +
                "\n4.Get groups list" +
                "\n5.Join group" +
                "\n6.Send file");
    }
}
