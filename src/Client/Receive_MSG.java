package Client;

import Client.MessageTypes.MSG_PONG;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.io.*;
import java.security.InvalidKeyException;

public class Receive_MSG implements Runnable {

    private InputStream input_stream;
    private Message.messageType type;
    private PrintWriter print_writer;
    private boolean show_server_messages;
    private String serverMessage;

    public Receive_MSG(InputStream input_stream, PrintWriter print_writer) {
        this.input_stream = input_stream;
        this.print_writer = print_writer;
    }

    @Override
    public void run() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(input_stream));

            while (true) {
                serverMessage = reader.readLine();

                if (serverMessage != null) {
                    obtainServerMessage(serverMessage);
                } else {
                    System.err.println("\nConnection with server was terminated");
                    System.exit(0);
                }

            }
        } catch (Exception e) {
            System.out.println("Server exception: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void obtainServerMessage(String serverMessage) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException{
        if (serverMessage.startsWith("PING")) {
            MSG_PONG messagePONG = new MSG_PONG();
            sendToServer(messagePONG.toString());
        } else if (serverMessage.startsWith("BCST")) {
            System.out.println("\n" + serverMessage);
            System.out.print("Answer: ");
        } else if (serverMessage.startsWith("FILE_TRANSFER")) {
            retrieveFile();
        } else if (serverMessage.startsWith("PRIVATE_MESSAGE")) {
            decryptPrivateMessage();
        } else {
            if (show_server_messages) {
                System.out.println("S: " + serverMessage);
                Message message = new Message(serverMessage);
                type = message.getMessageType();
            }
        }
    }

    private void retrieveFile() {
        String[] received = serverMessage.split(" ");
        try {
            FileOutputStream fos = new FileOutputStream("/Users/vpetrykov/Desktop/IdeaProjects/internet_technology/receive.txt");
            BufferedOutputStream bos = new BufferedOutputStream(fos);

            int file_size = Integer.parseInt(received[1]);
            byte[] myByteArray = new byte[file_size];
            System.out.println("file size is: " + myByteArray.length);
            input_stream.read(myByteArray, 0, myByteArray.length);

            bos.write(myByteArray, 0, myByteArray.length);
            bos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showServerMessages(boolean show_server_messages) {
        this.show_server_messages = show_server_messages;
    }

    public Message.messageType getType() {
        return type;
    }

    private void sendToServer(String message) {
        print_writer.println(message);
        print_writer.flush();
    }

    public String getServerMessage(int position) {
        String[] message = serverMessage.split(" ");
        return message[position];
    }

    private void decryptPrivateMessage() throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        String message_encrypted = getServerMessage(5);
        String entire_message = serverMessage;
        String message_decrypted = Client.decryptText(message_encrypted, Client.getPrivateKey());

        String[] entire_message_split = entire_message.split(" ");
        entire_message_split[5] = message_decrypted;

        StringBuilder return_message = new StringBuilder();

        for (int i = 1; i < entire_message_split.length; i++) {
            return_message.append(entire_message_split[i]).append(" ");
        }

        System.out.println(" " + return_message);
    }

}