package Client;

public class Message {

    enum messageType {
        HELO,
        BROADCAST,
        ALREADY_EXISTS,
        INVALID_FORMAT,
        UNKNOWN,
        DSCN,
        MENU,
        KICKED,
        FILE_TRANSFER,
        PUBLIC_KEY,
        PRIVATE_MESSAGE
    }

    private String message;

    public Message(String line) {
        this.message = line;
    }

    public messageType getMessageType() {

        if (message.startsWith("HELO")) {
            return messageType.HELO;
        } else if (message.equals("-ERR user already logged in")) {
            return messageType.ALREADY_EXISTS;
        } else if (message.equals("-ERR username has an invalid format (only characters, numbers and underscores are allowed)")){
            return messageType.INVALID_FORMAT;
        } else if (message.startsWith("+OK")) {
            return messageType.BROADCAST;
        } else if (message.startsWith("DSCN")){
            return messageType.DSCN;
        }else if(message.startsWith("MENU")){
            return messageType.MENU;
        }else if(message.startsWith("KICKED")){
            return messageType.KICKED;
        }else if(message.startsWith("FILE_TRANSFER")){
            return messageType.FILE_TRANSFER;
        }else if(message.startsWith("PUBLIC_KEY")){
            return messageType.PUBLIC_KEY;
        }else if(message.startsWith("PRIVATE_MESSAGE")){
            return messageType.PRIVATE_MESSAGE;
        }

        return messageType.UNKNOWN;
    }

}
